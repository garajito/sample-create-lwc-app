This application was created using,

https://github.com/muenzpraeger/create-lwc-app

(I don't know why I created a simple server, since it does nothing with the client. - gl)

There can be a lot of confusion in searches and documentations regarding SF front end development, since there is a lot of history there and mixed nomenclatures. So stick with **only** the following nomenclatures.

Modern:

* lwc,

* lightning web components

Avoid: (close any pages that mention these as far as salesforce development)

* aura

* force, or any derivative

* lightning components

It's important to realize that there is  `lwc` development on Salesforce Platform versus OSS. 'Google' them together.

Stick with the following pattern..



SF module resolution is a simple split of 2 parts, https://github.com/salesforce/lwc/blob/master/packages/%40lwc/module-resolver/src/resolve-module.ts#L83-L88



Some links:

prefer these examples.. https://recipes.lwc.dev/


but here, these are the official SF docs.. https://developer.salesforce.com/docs/component-library/bundle/lightning-accordion/example , 
- see only! `Lightning Web Components > COMPONENTS > lightning` thread, starting with this Accordion, https://developer.salesforce.com/docs/component-library/bundle/lightning-accordion

It's important to know, that all lwc components are generally open source and available to reuse or modify, etc, https://github.com/salesforce/base-components-recipes/tree/master/force-app/main/default/lwc



keep following below..


# temp-lwc-app

Here will be some information about the app.

## How to start?

Start simple by running `yarn watch` (or `npm run watch`, if you set up the project with `npm`). This will start the project with a local development server.

The source files are located in the [`src`](./src) folder. All web components are within the [`src/client/modules`](./src/modules) folder. The folder hierarchy also represents the naming structure of the web components. The entry file for the custom Express configuration can be found in the ['src/server'](./src/server) folder.

Find more information on the main repo on [GitHub](https://github.com/muenzpraeger/create-lwc-app).
